from django.contrib import admin
from .models import *
admin.site.register([Admin,Employer,JobSeeker,JobCategory,Job,JobApplication,Subscriber])