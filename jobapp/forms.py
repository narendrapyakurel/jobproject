from django import forms
from django_summernote.widgets import SummernoteWidget
from .models import *


class JobSeekerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = JobSeeker
        fields = ['username', 'password', 'confirm_password', 'name', 'address',
                  'mobile', 'image', 'qualification', 'skills', 'about', 'cv']

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                'JobSeeker with this user name already exitst please choose different name')
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        c_password = self.cleaned_data.get('confirm_password')
        if password != c_password:
            raise forms.ValidationError('password didnot match')
        return c_password


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class JobApplyForm(forms.ModelForm):
    class Meta:
        model = JobApplication
        fields = ['cover_letter']


class EmployerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Employer
        fields = ['username', 'password', 'confirm_password', 'name', 'mobile', 'email', 'address',
                  'image', 'website', 'compnay', 'company_image']

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                'JobSeeker with this user name already exitst please choose different name')
        return username
    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        c_password = self.cleaned_data.get('confirm_password')
        if password != c_password:
            raise forms.ValidationError('password didnot match')
        return c_password

class EmployerJobForm(forms.ModelForm):
    class Meta:
        model=Job
        exclude=['employer']
        widgets={
        'title':forms.TextInput(attrs={'placeholder':"enter job title here..",'id':'jobtitle',}),
        'category':forms.Select(attrs={'id':'jobcategory','class':'form-control'}),
        'details':SummernoteWidget()

        }
class SubscriberForm(forms.ModelForm):
    class Meta:
        model=Subscriber
        fields=['email']