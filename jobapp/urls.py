from django.urls import path
from .views import *
app_name = 'jobapp'

urlpatterns = [
    path('', JobSeekerHomeView.as_view(), name='jobseekerhome'),
    path('employer/', EmployerHomeView.as_view(), name='employerhome'),
    path('employer/profile/',EmployerProfileView.as_view(),name='employerprofile'),
    path('job-admin/', AdminHomeView.as_view(), name='adminhome'),
    path('jobseeker/registration/',
         JobSeekerRegistrationView.as_view(), name='jobseekerregister'),
    path('job/list/', JobListView.as_view(), name='joblist'),
    path('job/<int:pk>/detail/', JobDetailView.as_view(), name='jobdetail'),
    path('job/<int:pk>/apply/', JobSeekerJobApplyView.as_view(),
         name='jobseekerjobapply'),
    path('login/', LoginView.as_view(), name='login'),
    path('jobseeker/profile/', JobSeekerProfileView.as_view(),
         name='jobseekerprofile'),
    path('employer/registration', EmployerRegistrationView.as_view(),
         name='employerregistration'),
    path('employer/<int:pk>/detail/',EmployerDetailView.as_view(),name='employerdetail'),
    path('employer/job/<int:pk>/detail/',EmployerJobDetailView.as_view(),name='employerjobdetail'),
    path('employer/job/post/',EmployerJobCreateView.as_view(),name='employerjobcreate'),
    path('admin/profile/',AdminProfileView.as_view(),name='adminprofile'),
    path('logout/',LogoutView.as_view(),name='logout'),
    path('admin/employer/list/',AdminEmployerListView.as_view(),name='adminemployerlist'),
    path('admin/employer/<int:pk>/detail/',AdminEmployerDetailView.as_view(),name='adminemployerdetail'),
    path('admin/jobseeker/list/',AdminJobseekerListView.as_view(),name='adminjobseekerlist'),
    path('admin/jobseeker/<int:pk>/detail/',AdminJobSeekerDetailView.as_view(),name='adminjobseekerdetail'),
    path('admin/job/list/',AdminJobListView.as_view(),name='adminjoblist'),
    path('admin/job/<int:pk>/detail/',AdminJobDetailView.as_view(),name='adminjobdetail'),
    path('subscribe/',JobSeekerSubscribeView.as_view(),name='jobseekersubscribe'),
    path('subscriber/check/',SubscriberCheckView.as_view(),name='subscribercheck'),
    path('job-api/job/list/',ApiJobListView.as_view(),name='apijoblist'),
    path('job-api/job/<int:pk>/update/',ApiJobUpdateView.as_view(),name='apijobupdateview'),
]
    