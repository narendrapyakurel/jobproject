from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.conf import settings
from rest_framework.generics import *
from .serializers import *


class EmployerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name='employer').exists():
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)


class AdminReqiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name='admin').exists():
            pass
        else:
            return redirect('jobapp:login')
        return super().dispatch(request, *args, **kwargs)


class JobSeekerHomeView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerhome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # jobseeker_id = self.kwargs['pk']
        # jobseeker = JobSeeker.objects.get(id=jobseeker_id)
        # jobseeker.views_count += 1
        # jobseeker.save()
        context['subform'] = SubscriberForm
        return context


class EmployerHomeView(EmployerRequiredMixin, TemplateView):
    template_name = 'employertemplates/employerhome.html'


class AdminHomeView(AdminReqiredMixin, TemplateView):
    template_name = 'admintemplates/adminhome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allemployers'] = Employer.objects.all()
        context['alljobs'] = Job.objects.all()
        context['alljobseekers'] = JobSeeker.objects.all()
        context['allapplications'] = JobApplication.objects.all()
        return context


class JobSeekerRegistrationView(SuccessMessageMixin,CreateView):
    template_name = 'jobseekertemplates/jobseekerregisteration.html'
    form_class = JobSeekerForm
    success_url = '/'
    success_message='registration succesively'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = User.objects.create_user(uname, '', pword)
        form.instance.user = user
        login(self.request, user)
        return super().form_valid(form)


class JobListView(ListView):
    template_name = 'jobseekertemplates/joblist.html'
    queryset = Job.objects.all()
    context_object_name = 'joblist'


class JobDetailView(DetailView):
    template_name = 'jobseekertemplates/jobdetail.html'
    queryset = Job.objects.all()
    context_object_name = 'jobdetail'


class JobSeekerJobApplyView(CreateView):
    template_name = 'jobseekertemplates/jobseekerjobapply.html'
    form_class = JobApplyForm
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.groups.first().name == 'jobseeker':
            pass
        else:
            return redirect('/login/')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        job_id = self.kwargs['pk']
        job = Job.objects.get(id=job_id)
        user = self.request.user
        job_seeker = JobSeeker.objects.get(user=user)
        form.instance.job = job
        form.instance.jobseeker = job_seeker
        return super().form_valid(form)


class LoginView(FormView):
    template_name = 'jobseekertemplates/login.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = authenticate(username=uname, password=pword)
        self.thisuser = user
        if user is not None and user.groups.exists():
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {'form': form, 'error': 'you are not authorized'})
        return super().form_valid(form)

    def get_success_url(self):
        user = self.thisuser
        if user.groups.filter(name='jobseeker').exists():
            return reverse('jobapp:jobseekerhome')
        elif user.groups.filter(name='employer').exists():
            return reverse('jobapp:employerhome')
        elif user.groups.filter(name='admin').exists():
            return reverse('jobapp:adminhome')
        else:
            return reverse('jobapp:login')


class JobSeekerProfileView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerprofile.html'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.first().name == 'jobseeker':
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        jobseeker = JobSeeker.objects.get(user=logged_user)
        context['jobseeker'] = jobseeker
        return context


class EmployerRegistrationView(CreateView):
    template_name = 'employertemplates/employerregistration.html'
    form_class = EmployerForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = User.objects.create_user(uname, '', pword)
        form.instance.user = user

        return super().form_valid(form)


class EmployerProfileView(EmployerRequiredMixin, TemplateView):
    template_name = 'employertemplates/employerprofile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        employer = Employer.objects.get(user=logged_user)
        context['employer'] = employer
        return context


class EmployerDetailView(DetailView):
    template_name = 'employertemplates/employerdetail.html'
    model = Employer
    context_object_name = 'employerdetail'


class EmployerJobDetailView(EmployerRequiredMixin, DetailView):
    template_name = 'employertemplates/employerjobdetail.html'
    model = Job
    context_object_name = 'jobobject'


class EmployerJobCreateView(EmployerRequiredMixin, CreateView):
    template_name = 'employertemplates/employerjobcreate.html'
    form_class = EmployerJobForm
    success_url = reverse_lazy('jobapp:employerprofile')

    def form_valid(self, form):
        employer = Employer.objects.get(user=self.request.user)
        form.instance.employer = employer
        to_emails=[]
        for subscriber in Subscriber.objects.all():
            email=subscriber.email
            to_emails.append(email)
        print(to_emails,'****')
        send_mail('newjob posted','please http://localhost:8000',settings.EMAIL_HOST_USER,to_emails,fail_silently=False)
        return super().form_valid(form)


class AdminProfileView(AdminReqiredMixin, TemplateView):
    template_name = 'admintemplates/adminprofile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        admin = Admin.objects.get(user=logged_user)
        context['admin'] = admin
        context['allemployers'] = Employer.objects.all()
        context['alljobseekers'] = JobSeeker.objects.all()
        return context


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class AdminEmployerListView(ListView):
    template_name = 'admintemplates/adminemployerlist.html'
    model = Employer
    context_object_name = 'adminemployer'


class AdminEmployerDetailView(DetailView):
    template_name = 'admintemplates/adminemployerdetail.html'
    model = Employer
    context_object_name = 'adminemployerobject'


class AdminJobseekerListView(ListView):
    template_name = 'admintemplates/adminjobseekerlist.html'
    model = JobSeeker
    context_object_name = 'jobseekerobject'


class AdminJobSeekerDetailView(DetailView):
    template_name = 'admintemplates/adminjobseekerdetail.html'
    model = JobSeeker
    context_object_name = 'jobseekerdetailobject'


class AdminJobListView(ListView):
    template_name = 'admintemplates/adminjoblist.html'
    model = Job
    context_object_name = 'jobobject'


class AdminJobDetailView(DetailView):
    template_name = 'admintemplates/adminjobdetail.html'
    model = Job
    context_object_name = 'jobdetailobject'


class JobSeekerSubscribeView(SuccessMessageMixin,CreateView):
    template_name = 'jobseekertemplates/error.html'
    form_class = SubscriberForm
    success_url = '/'
    success_message='thank u for subscribing'
   

    def form_valid(self, form):
        email = form.cleaned_data['email']
        if Subscriber.objects.filter(email=email).exists():
            return render(self.request, self.template_name, {'error': 'subscriber already exits'})
        send_mail('subscription', 'thanks for subscription job site',
              settings.EMAIL_HOST_USER, [email,], fail_silently=False)    

        return super().form_valid(form)


class SubscriberCheckView(View):
    def get(self, request):
        email = request.GET.get('email')
        if Subscriber.objects.filter(email=email).exists():
            return JsonResponse({'error': 'error'})
        else:
            return JsonResponse({'error':'success'})
class ApiJobListView(ListCreateAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
class ApiJobUpdateView(RetrieveUpdateDestroyAPIView):
    queryset=Job.objects.all()
    serializer_class=JobSerializer
